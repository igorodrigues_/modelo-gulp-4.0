const { src, dest, watch, parallel, series, task } = require("gulp");
const concatCSS = require("gulp-concat-css");
const csso = require("gulp-csso");
const babel = require("gulp-babel");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");
const imagemin = require("gulp-imagemin");

//source
const dirImage = "./images/*";
const dirCSS = ["./css/*.css", "./css/**/*.css"];
const dirJS =["./js/*.js", "./js/**/*.js"];

//dist
const dirBuild = "./dist/";
const dirBuildImage = "./images/";

// Otimizando CSS (sem compilar)
task("css", function () {
  return src(dirCSS)
    .pipe(concatCSS("all.min.css"))
    .pipe(csso())
    .pipe(dest(dirBuild));
});

// Otimizando JS
task("js", function () {
  return src(dirJS)
    .pipe(babel({ presets: ["@babel/env"] }))
    .pipe(concat("all.min.js"))
    .pipe(uglify())
    .pipe(dest(dirBuild));
});

// Otimixando Image
task("images", function () {
  return src(dirImage)
    .pipe(imagemin({ progressive: true, optimizationLevel: 5 }))
    .pipe(dest(dirBuildImage));
});

// Monitora arquivo e executando tarefas..
task("watched", function () {
  watch(
    ["./css/*.css", "./css/**/*.css", "./js/*.js", "./js/**/*.js"],
    parallel(["css", "js"])
  );
});

//Executando tarefas
task("default", series(["css", "js", "images", "watched"]));
